package src;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;  // adds regex ability

/**
 *
 * project name: Main
 package name: src
 file name: Main
 project version: default
 Created:
 Dec 8, 2017, 7:48:19 AM
 * @author bumerrrang
 */
public class Main {
    
    // by default values of objects are 'null', of intgers are '0'
    private static String protocol;
    private static String host;
    private static int port;
    private static String path;
    private static String query;

    // variables are used for testing/comparing results
    // as it is a test exercise, we need to test it-)
    private static String protocol_test;
    private static String host_test;
    private static int port_test;
    private static String path_test;
    private static String query_test;
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        URL url;
        try {
            url = new URL(args[0].trim());
            setProtocol(url.getProtocol());
            setPort(url.getPort());
            setHost(url.getHost());
            setPath(url.getPath().substring(1));
            setQuery(url.getQuery());
        } catch (MalformedURLException ex) {
            System.err.print("Wong URL was given");
        }


        System.out.println("Protocol: " + getProtocol());
        System.out.println("Host: " + getHost());
        System.out.println("Port: " + getPort());
        System.out.println("Path: " + getPath().substring(1));
        System.out.println("Query: " + getQuery());
        long start = System.nanoTime();  // defines the start poin on timer
        for(int i = 0; i < 10000; i++) {
            regexParsing(args[0]);
        }
        System.out.println("Regex: " + ((System.nanoTime() - start) / 1000000) + "msecs");
        
        start = System.nanoTime();  // defines the start poin on timer
        for(int i = 0; i < 10000; i++) {
            stateParsing(args[0]);
        }
        System.out.println("State: " + ((System.nanoTime() - start) / 1000000) + "msecs");
    }
    
    // this static method will iterate defined number of times using regular expression logic and return number of milliseconds it spent for the operation
    static void regexParsing(String url) {
        setProtocol_test(url.split("://")[0]);  // gets protocol, uses regex
        url = url.split("://")[1];  // gets second been splitted substring
        
        // if given url contains port number
        if(url.contains(":")) {
            setHost_test(url.split(":")[0]);  // gets host name
            url = url.split(":")[1];  // gets substrig, uses regex
            //gets port
            setPort_test(Integer.parseInt(url.substring(0, url.indexOf('/'))));
            url = url.substring(url.indexOf("/"));
        }
        else {
            setHost_test(url.split("/")[0]);  // gets host, uses regex
            url = url.split("/")[1];  // gets host, uses regex
        }
        
        setPath_test(url.split("\\?")[0].substring(1));  // gets path, uses regex
        url = url.split("\\?")[1];
        
        setQuery_test(url);  // gets params, uses only the rest part of url

            // starts with scheme, whatever sequence of characters (yes, it allows custom schemes)
            // then required delimeter '://'
            // then is possibla a subdomain name, allowed custom subdomain name even with characters '+&@#/%?=~_|!:,.;' in it
            // then required custom domain name, which may consist of some arbitrary characters, which ends with '.' and some word character(s)
            // then not required port number whith ':' before that
            // then required '/path' part
            // then not required 'Query String', which also could consists of custom characters [-a-zA-Z0-9+&@#%=~_|!:,;]] with '/' in the start
//            String regex2 = "^[a-zA-Z0-9_]+://[-a-zA-Z0-9+&@#/%?=~_|!,.;]*[-a-zA-Z0-9+&@#/%?=~_|!,;]+[.][a-zA-Z0-9_]+(:(\\d)?)*/path(\\?([-a-zA-Z0-9+&@#%=~_|!,;]*))?$";
//            String text = "https://2_56jhgdc|=v&.rt5/path?52354jhfhgdch___KHFCHGDGFS%%&%&";
//            String text2 = "scheme://domain.com:8/path?query_string";

//            try {
//                Pattern patt = Pattern.compile(regex2);
//                Matcher matcher = patt.matcher(text2);
//                if(matcher.matches()) {
//                   System.out.println("Matching!");
//                    return 0;
//                }
//            } catch (RuntimeException e) {
//                System.out.println("NO Matching!");
//                return 0;
//            }
//            System.out.println("NO Matching!");
//            return 0;
        
    }
    
    // this static method will iterate defined number of times using state machine logic and return number of milliseconds it spent for the operation
    static void stateParsing(String url) {
        setProtocol_test(url.substring(0, url.indexOf(':')));

        url = url.substring(url.indexOf("://")+3);  // gets url without schema

        if(url.contains(":")) {  // port was defined in url
            setHost_test(url.substring(0, url.indexOf(":")));  // gets host
            setPort_test(Integer.parseInt(url.substring(url.indexOf(":")+1, url.indexOf("/"))));  // gets port
            url = url.substring(url.indexOf("/"));  // edits url
        }
        else {
            setHost_test(url.substring(0, url.indexOf("/")));  // gets host
            url = url.substring(url.indexOf("/"));  // edits url
        }

        setPath_test(url.substring(1, url.indexOf("?")));
        url = url.substring(url.indexOf("?")+1);

        setQuery_test(url);
    }

    /**
     * @return the protocol
     */
    public static String getProtocol() {
        return protocol;
    }

    /**
     * @param aProtocol the protocol to set
     */
    public static void setProtocol(String aProtocol) {
        protocol = aProtocol;
    }

    /**
     * @return the host
     */
    public static String getHost() {
        return host;
    }

    /**
     * @param aHost the host to set
     */
    public static void setHost(String aHost) {
        host = aHost;
    }

    /**
     * @return the port
     */
    public static int getPort() {
        return port;
    }

    /**
     * @param aPort the port to set
     */
    public static void setPort(int aPort) {
        port = aPort;
    }

    /**
     * @return the path
     */
    public static String getPath() {
        return path;
    }

    /**
     * @param aPath the path to set
     */
    public static void setPath(String aPath) {
        path = aPath;
    }

    /**
     * @return the query
     */
    public static String getQuery() {
        return query;
    }

    /**
     * @param aQuery the query to set
     */
    public static void setQuery(String aQuery) {
        query = aQuery;
    }

    /**
     * @return the protocol_test
     */
    public static String getProtocol_test() {
        return protocol_test;
    }

    /**
     * @param aProtocol_test the protocol_test to set
     */
    public static void setProtocol_test(String aProtocol_test) {
        protocol_test = aProtocol_test;
    }

    /**
     * @return the host_test
     */
    public static String getHost_test() {
        return host_test;
    }

    /**
     * @param aHost_test the host_test to set
     */
    public static void setHost_test(String aHost_test) {
        host_test = aHost_test;
    }

    /**
     * @return the port_test
     */
    public static int getPort_test() {
        return port_test;
    }

    /**
     * @param aPort_test the port_test to set
     */
    public static void setPort_test(int aPort_test) {
        port_test = aPort_test;
    }

    /**
     * @return the path_test
     */
    public static String getPath_test() {
        return path_test;
    }

    /**
     * @param aPath_test the path_test to set
     */
    public static void setPath_test(String aPath_test) {
        path_test = aPath_test;
    }

    /**
     * @return the query_test
     */
    public static String getQuery_test() {
        return query_test;
    }

    /**
     * @param aQuery_test the query_test to set
     */
    public static void setQuery_test(String aQuery_test) {
        query_test = aQuery_test;
    }
}