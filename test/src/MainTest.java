/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bumerrrang
 */
public class MainTest {
    
    public MainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Main.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = {"http://host:8090/path?params"};
        Main.main(args);
        
        // with equal start conditional 'url' arguments must be equal
        assertEquals(Main.getProtocol(), Main.getProtocol_test());
        assertEquals(Main.getHost(), Main.getHost_test());
        assertEquals(Main.getPort(), Main.getPort_test());
        assertEquals(Main.getPath(), Main.getPath_test());
        assertEquals(Main.getQuery(), Main.getQuery_test());
    }

    /**
     * Test of regexParsing method, of class Main.
     */
    @Test
    public void testRegexParsing() {
        System.out.println("regexParsing");
        String[] args = {"http://host:8090/path?params"};
        Main.main(args);  // defines half of static arguments
        Main.regexParsing(args[0]);  // defines anoter half of arguments
        
        // with equal start conditional 'url' arguments must be equal
        assertEquals(Main.getProtocol(), Main.getProtocol_test());
        assertEquals(Main.getHost(), Main.getHost_test());
        assertEquals(Main.getPort(), Main.getPort_test());
        assertEquals(Main.getPath(), Main.getPath_test());
        assertEquals(Main.getQuery(), Main.getQuery_test());
    }

    /**
     * Test of stateParsing method, of class Main.
     */
    @Test
    public void testStateParsing() {
        System.out.println("stateParsing");
        String[] args = {"http://host:8090/path?params"};
        Main.main(args);  // defines half of static arguments
        Main.stateParsing(args[0]);  // defines anoter half of arguments
        
        // with equal start conditional 'url' arguments must be equal
        assertEquals(Main.getProtocol(), Main.getProtocol_test());
        assertEquals(Main.getHost(), Main.getHost_test());
        assertEquals(Main.getPort(), Main.getPort_test());
        assertEquals(Main.getPath(), Main.getPath_test());
        assertEquals(Main.getQuery(), Main.getQuery_test());
    }
}